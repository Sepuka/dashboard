<?php

require 'vendor/autoload.php';

$httpClient = new GuzzleHttp\Client();
$headers = [
    // подумать над сжатием т.к. может отразится на скорости получения окончательного ответа
    'user-agent' => 'TimeWeb OPR dashboard inspector v0.1',
    'pragma' => 'no-cache',
    'accept-encoding' => 'gzip, deflate, sdch, br',
    'accept-language' => 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
    'upgrade-insecure-requests' => '1',
    'x-compress' => 'null',
    'user-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
    'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'cache-control' => 'no-cache',
    'authority' => 'hosting.timeweb.ru',
    'cookie' => 'PHPSTAFFSESSID=44bc992ffbe41b4086314339c728811e; _identity_staff=7ef9fd7ea1d0f71475bf2511f038b61a2a19307a3da3478e5e7040f339910486a%3A2%3A%7Bi%3A0%3Bs%3A15%3A%22_identity_staff%22%3Bi%3A1%3Bs%3A61%3A%22%5B%22m.shlomin%22%2C%226391e2e2-7596-4305-955b-6045b765e97b%22%2C62208000%5D%22%3B%7D; _csrf=defe759fec23f61f031089234517cdc1efc0a6e8131851d9fce5d66229749cc1a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22KoBmW1na5QaxbgHmHfd8I75in--YRERa%22%3B%7D; _ym_uid=1475779928155138712; menu_viewed_new_item_20=09fc133f41cde626ccd3ad595c4fe028a1b68318ba4d2d97d541498e29f7f369a%3A2%3A%7Bi%3A0%3Bs%3A23%3A%22menu_viewed_new_item_20%22%3Bi%3A1%3Bi%3A1%3B%7D; _identity_vds=38f7085f2e3173bb24841995b422cd35e203d467d68872793f33731160b854c4a%3A2%3A%7Bi%3A0%3Bs%3A13%3A%22_identity_vds%22%3Bi%3A1%3Bs%3A59%3A%22%5B%22cn90632%22%2C%226391ed1b-3d03-436c-9023-dc81ad0c901b%22%2C62208000%5D%22%3B%7D; master_login=maksim1681; PHPSESSID=f7a73d9bc749d1f86566db63b565dbbc; _ym_isad=1; _identity_first=a55a1ede60edb17a4be8d55bafbd96a6ed20953a28eb3470d1870b13cf652cbaa%3A2%3A%7Bi%3A0%3Bs%3A15%3A%22_identity_first%22%3Bi%3A1%3Bs%3A59%3A%22%5B%22cn90632%22%2C%226391ed1b-3d03-436c-9023-dc81ad0c901b%22%2C62208000%5D%22%3B%7D; _ga=GA1.2.1456424083.1475779928; _identity_hosting=da5f863def702cecc4410ace12cc7a073b836cf943d7a9b913e3341ac101b5b7a%3A2%3A%7Bi%3A0%3Bs%3A17%3A%22_identity_hosting%22%3Bi%3A1%3Bs%3A62%3A%22%5B%22maksim1681%22%2C%2263915c87-b7e1-4fcc-98ce-60a3112a16cb%22%2C62208000%5D%22%3B%7D; lt_uid=fe136137-26d4-477a-adae-ec7abbcfbe59'
];
$httpRequest = new \GuzzleHttp\Psr7\Request('get', 'https://hosting.timeweb.ru/', $headers);
$start = microtime(true);
$response = $httpClient->send($httpRequest);
$stop = microtime(true);
$result = $stop - $start;

$fp = fopen('data.tsv', 'a');
fwrite($fp, time()."\t".$result."\n");
fclose($fp);