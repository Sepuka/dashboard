const NODE_ENV = process.env.NODE_ENV || 'development';
const OUTPUT_PATH = process.env.OUTPUT_PATH || __dirname + '/public';

module.exports = {
    context: __dirname + '/assets/js',
    entry: './app.js',
    output: {
        path: OUTPUT_PATH,
        filename: 'bundle.js'
    },
    watch: NODE_ENV == 'development',
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: 'style!css!'
            },{
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },{
                test: /\.json$/,
                loader: 'json-loader'
            }
        ]
    },
    devtool: NODE_ENV == 'development' ? 'source-map' : null
}
