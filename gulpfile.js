'use strict';

var gulp = require('gulp');

gulp.task('js',function(){
    gulp.src('./assets/js/*.js')
        .pipe(gulp.dest('./public/js/'));
});

gulp.task('default', ['js']);
