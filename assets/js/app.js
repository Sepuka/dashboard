'use strict';

import '../css/style.css';

import React from 'react';
import ReactDOM from 'react-dom';
import HostingSpeed from './hosting_speed';
import Uptime from './uptime';

function Dashboard() {
    return <div>
            <HostingSpeed width={960} height={500} />
            <Uptime />
        </div>;
}

ReactDOM.render(<Dashboard />, document.getElementById('dashboard'));
