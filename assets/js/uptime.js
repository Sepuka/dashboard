import React from 'react';

class Uptime extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            uptime: 0,
            error: ''
        };
        this.tick();
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            30000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        const component = this;

        fetch('/dashboard.php')
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                component.setState({
                    uptime: json.uptime,
                    error: ''
                });
            }).catch(function (ex) {
                component.setState({
                    error: ex.toString()
                });
            });
    }

    render() {
        return <div id='uptime'>
            <div className='componentValue'>{this.state.uptime}</div>
            <div className='componentError'>{this.state.error}</div>
        </div>;
    }
}

module.exports = Uptime;
