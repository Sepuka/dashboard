import React from 'react';
import {Chart} from 'react-d3-core';
import {LineChart} from 'react-d3-basic';

class HostingSpeed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: require('../../data.json'),
            yLabel: "Time in sec",
            heightScale: 'linear',
            widthScale: 'time'
        };
    }

    render() {
        const chartSeries = [
            {
                field: 'tfb',
                name: 'tfb',
                color: '#ff7f0e'
            }
        ];
        return <LineChart
                chartSeries={chartSeries}
                data={this.state.data}
                width={this.props.width}
                height={this.props.height}
                x={(d) => new Date(d.timestamp * 1000)}
                xScale={this.state.widthScale}
                yScale={this.state.heightScale}
                yLabel={this.state.yLabel}
            />;
        //

        //
        // const yAxis = d3.axisLeft()
        //     .scale(heightScale);
        // this.getUI('canvas').append('g')
        //     .attr('transform', 'translate(25, 10)')
        //     .call(yAxis);
        //
        // const minWidth = d3.min(this.state.data, function (d) {
        //     return d.timestamp;
        // });
        // const maxWidth = d3.max(this.state.data, function (d) {
        //     return d.timestamp;
        // });
        //
        // const widthScale = d3.scaleLinear()
        //     .range([30, this.props.width])
        //     .domain([minWidth, maxWidth]);
        //
        // const xAxis = d3.axisBottom()
        //     .scale(widthScale);
        //
        // this.getUI('canvas').canvas.append('g')
        //     .attr('transform', 'translate(0, ' + 480 + ')')
        //     .call(xAxis);
        // const line = d3.line()
        //     .x(function (d) {
        //         return widthScale(d.timestamp)
        //     })
        //     .y(function (d) {
        //         return heightScale(d.tfb)
        //     });
        // this.getUI('canvas').append('path')
        //     .datum(this.state.data)
        //     .attr('class', 'line')
        //     .attr('d', line);
    }
}

module.exports = HostingSpeed;
