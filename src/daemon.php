<?php

namespace dashboard;

use dashboard\components\Di;
use sepuka\daemon\Daemon;

define('PROJECT_DIR', __DIR__.'/..');

require PROJECT_DIR.'/vendor/autoload.php';

$config = require PROJECT_DIR.'/config/config.php';
$di = new Di(__DIR__.'/../config', 'config.yml');
$app = new Collector($config, $di);
$daemon = new Daemon();
$daemon->start($app);
