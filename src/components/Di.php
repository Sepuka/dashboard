<?php

namespace dashboard\components;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class Di
{
    /**
     * @var string
     */
    private $path;
    /**
     * @var string
     */
    private $config;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(string $path, string $config)
    {
        $this->config = $config;
        $this->path = $path;
        $this->init();
    }

    public function init()
    {
        $this->container = new ContainerBuilder();
        $fileLoader = new YamlFileLoader($this->container, new FileLocator([$this->path]));
        $fileLoader->load($this->config);
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }
}
