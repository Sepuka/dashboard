<?php

declare(strict_types=1);

namespace dashboard\components;

use dashboard\metrics\WorkerPoolInterface;
use sepuka\daemon\DaemonInterface;

class WorkerPoolFactory implements WorkerPoolFactoryInterface
{
    /** @var WorkerPoolInterface[] */
    private $instances = [];

    public function instance(string $poolName, array $config, DaemonInterface $daemon): WorkerPoolInterface
    {
        if (!isset($this->instances[$poolName])) {
            $this->instances[$poolName] = new $poolName($config, $daemon);
        }

        return $this->instances[$poolName];
    }
}
