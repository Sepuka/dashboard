<?php

declare(strict_types=1);

namespace dashboard\components;

use dashboard\metrics\WorkerPoolInterface;
use sepuka\daemon\DaemonInterface;

interface WorkerPoolFactoryInterface
{
    public function instance(string $poolName, array $config, DaemonInterface $daemon): WorkerPoolInterface;
}
