<?php

declare(strict_types = 1);

namespace dashboard\metrics\uptime;

use dashboard\metrics\MetricsInterface;

class Uptime implements MetricsInterface
{
    private const BASH_SUCCESS_CODE = 0;

    public function mesure()
    {
        exec('uptime', $output, $return_var);
        if ($return_var === self::BASH_SUCCESS_CODE) {
            return $this->parse($output[0]);
        }

        return -1;
    }

    private function parse(string $data): string
    {
        $parsed = explode(' ', $data, 10);

        return $parsed[4];
    }
}
