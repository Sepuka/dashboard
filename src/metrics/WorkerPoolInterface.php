<?php

namespace dashboard\metrics;

use Psr\Log\LoggerInterface;
use sepuka\daemon\DaemonInterface;

interface WorkerPoolInterface
{
    public function __construct(array $resources, DaemonInterface $daemon);
    public function setLogger(LoggerInterface $logger);
    public function work(): array;
    public function mesure(): array;
}
