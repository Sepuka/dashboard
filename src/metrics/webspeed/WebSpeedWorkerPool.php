<?php

declare(strict_types=1);

namespace dashboard\metrics\webspeed;

use dashboard\metrics\AbstractWorkerPool;
use sepuka\daemon\DaemonInterface;

class WebSpeedWorkerPool extends AbstractWorkerPool
{
    /** @var array */
    private $resources = [];
    /** @var array */
    private $pidRegister = [];
    /** @var DaemonInterface */
    private $daemon;
    
    public function __construct(array $resources, DaemonInterface $daemon)
    {
        $this->resources = $resources;
        $this->daemon = $daemon;
    }

    public function mesure(): array
    {
        foreach ($this->resources as $resource) {
            $pid = $this->ensureForked();

            if ($this->isChildProcess($pid)) {
                $index = new WebSpeed($resource);
                $this->daemon->loop([$index, 'mesure']);
            } else {
                $this->pidRegister[] = $pid;
            }
        }

        return $this->pidRegister;
    }
}
