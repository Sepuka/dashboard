<?php

namespace dashboard\metrics\webspeed;

use dashboard\collector\TimeMeasurement;
use dashboard\metrics\MetricsInterface;

class WebSpeed implements MetricsInterface
{
    /**
     * @var string
     */
    private $addr;

    public function __construct(string $page)
    {
        $this->addr = $page;
    }

    public function mesure()
    {
        $measure = new TimeMeasurement($this->addr);
        $measure->start();
        file_get_contents($this->addr);
        $measure->stop();
        file_put_contents('/tmp/debug', $measure->getMeasure()."\n", FILE_APPEND);
    }
}
