<?php

declare(strict_types=1);

namespace dashboard\metrics;

use Psr\Log\LoggerInterface;

abstract class AbstractWorkerPool implements WorkerPoolInterface
{
    const FORK_FAILURE_FLAG = -1;
    const FORK_CHILD_PID = 0;

    /** @var LoggerInterface */
    protected $logger;

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function work(): array
    {
        $pid = $this->ensureForked();

        if ($this->isChildProcess($pid)) {
            return $this->mesure();
        }

        return [$pid];
    }

    abstract public function mesure(): array;

    protected function isChildProcess($pid):bool
    {
        return $pid === self::FORK_CHILD_PID;
    }

    protected function ensureForked(): int
    {
        $pid = pcntl_fork();

        if ($pid == self::FORK_FAILURE_FLAG) {
            $msg = sprintf('Fork %s failed.', __CLASS__);
            $this->logger->emergency($msg);
            throw new \Exception($msg);
        }

        if ($pid) {
            $this->logger->info("fork $pid process.");
        }

        return $pid;
    }
}
