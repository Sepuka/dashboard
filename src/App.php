<?php

namespace dashboard;

use dashboard\metrics\uptime\Uptime;

class App
{
    public function run()
    {
        $uptime = new Uptime();
        return json_encode([
            'uptime' => $uptime->mesure()
        ]);
    }
}
