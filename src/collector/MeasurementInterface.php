<?php
namespace dashboard\collector;

interface MeasurementInterface
{
    public function __construct(string $resource);
    public function getResource(): string;
    public function getTime(): \DateTimeInterface;
    public function getMeasure();
}
