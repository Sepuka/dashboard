<?php

namespace dashboard\collector;

interface CollectorInterface
{
    public function save(MeasurementInterface $measurement);
}
