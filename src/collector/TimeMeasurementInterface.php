<?php

namespace dashboard\collector;

interface TimeMeasurementInterface extends MeasurementInterface
{
    public function start();
    public function stop();
}
