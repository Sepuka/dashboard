<?php

namespace dashboard\collector;

class TimeMeasurement implements TimeMeasurementInterface
{
    private $resource;
    private $startTime;
    private $endTime;

    public function __construct(string $resource)
    {
        $this->resource = $resource;
    }

    public function start()
    {
        if ($this->startTime) {
            throw new \LogicException('Measurement already started!');
        }

        $this->startTime = microtime(true);
    }

    public function stop()
    {
        if ($this->endTime) {
            throw new \LogicException('Measurement already ended!');
        }

        $this->endTime = microtime(true);
    }

    /**
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @return float
     */
    public function getMeasure(): float
    {
        return $this->endTime - $this->startTime;
    }

    public function getTime(): \DateTimeInterface
    {
        $date = new \DateTime();
        $date->setTimestamp($this->endTime);

        return $date;
    }
}
