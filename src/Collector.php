<?php

namespace dashboard;

use dashboard\components\Di;
use dashboard\components\WorkerPoolFactory;
use dashboard\components\WorkerPoolFactoryInterface;
use dashboard\metrics\webspeed\WebSpeedWorkerPool;
use Psr\Log\LoggerInterface;
use sepuka\daemon\CollectorInterface;
use sepuka\daemon\DaemonInterface;

class Collector implements CollectorInterface
{
    /** @var array */
    private $config = [];
    /** @var WorkerPoolFactoryInterface */
    private $workerPoolFactory;
    /** @var array */
    private $pidRegister = [];
    /** @var LoggerInterface */
    private $log;
    /** @var DaemonInterface */
    private $daemon;
    /** @var Di */
    private $di;

    /**
     * @param array $config
     * @param Di    $di
     */
    public function __construct(array $config, Di $di)
    {
        $this->config = $config;
        $this->di = $di;
        $this->init();
    }

    public function __destruct()
    {
        foreach ($this->pidRegister as $pid) {
            posix_kill($pid, SIGTERM);
            $this->log->info("Stopping {$pid} process.");
        }
    }

    public function init()
    {
        $this->workerPoolFactory = new WorkerPoolFactory();
        $this->log = $this->di->getContainer()->get('logger');
    }

    public function run(DaemonInterface $daemon)
    {
        $this->daemon = $daemon;
        $this->webSpeed();
    }

    private function webSpeed()
    {
        $pages = $this->config['webSpeed'];
        if ($pages) {
            /** @var WebSpeedWorkerPool $workerPool */
            $workerPool = $this->workerPoolFactory->instance(WebSpeedWorkerPool::class, $pages, $this->daemon);
            $workerPool->setLogger($this->log);
            $pid = $workerPool->work();
            $this->registerChild($pid);
        }
    }

    private function registerChild(array $pidList)
    {
        $this->pidRegister = $pidList;
    }
}
