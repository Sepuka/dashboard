<?php

use dashboard\App;

require __DIR__.'/../vendor/autoload.php';

$app = new App();
echo $app->run();
